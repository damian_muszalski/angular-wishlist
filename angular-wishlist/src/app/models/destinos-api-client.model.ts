import { DestinoViaje } from './destino-viaje.model';
import { Observable } from 'rxjs';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes-state.model';
import { Store } from '@ngrx/store';
import { Appstate } from '../app.module';
import { Injectable } from '@angular/core';

@Injectable()
export class DestinosApiClient {
    destinosObs: Observable<DestinoViaje[]>

    constructor(private store: Store<Appstate>) {
        this.destinosObs = this.store.select<DestinoViaje[]>((state: Appstate) => state.destinos.items);
    }

    add(d: DestinoViaje): void {
        this.store.dispatch(new NuevoDestinoAction(d));
    }

    getAll(): Observable<DestinoViaje[]> {
        return this.destinosObs;
    }

    elegir(d: DestinoViaje) {
        this.store.dispatch(new ElegidoFavoritoAction(d));
    }

    isSelected(destino: DestinoViaje): Observable<boolean> {
        return this.store.select<boolean>((state: Appstate) => state.destinos.favorito == destino);
    }
}