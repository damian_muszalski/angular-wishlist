import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { DestinoViaje } from './destino-viaje.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// ESTADO
export interface DestinosViajesState {
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export const initializeDestinosViajeState = function () {
    return {
        items: [],
        loading: false,
        favorito: null
    };
};

//ACCIONES
export enum DestinosViajesActionTypes {
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    VOTE_UP = '[Destinos Viajes] Vote Up',
    VOTE_DOWN = '[Destinos Viajes] Vote Down',
    VOTE_RESET = '[Destinos Viajes] Reset'
}

export class NuevoDestinoAction implements Action {
    type = DestinosViajesActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje) { }
}

export class ElegidoFavoritoAction implements Action {
    type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje) { }
}

export class VoteUpAction implements Action {
    type = DestinosViajesActionTypes.VOTE_UP;
    constructor(public destino: DestinoViaje) { }
}

export class VoteDownAction implements Action {
    type = DestinosViajesActionTypes.VOTE_DOWN;
    constructor(public destino: DestinoViaje) { }
}

export class VoteResetAction implements Action {
    type = DestinosViajesActionTypes.VOTE_RESET;
    constructor(public destino: DestinoViaje) { }
}

export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction
| VoteUpAction | VoteDownAction | VoteResetAction;

//REDUCERS
export function reducerDestinosViajes(state: DestinosViajesState, action: DestinosViajesActions): DestinosViajesState {
    switch (action.type) {
        case DestinosViajesActionTypes.NUEVO_DESTINO: {
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino]
            };
        }
        case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
            const fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
            return {
                ...state,
                favorito: fav
            };
        }
        case DestinosViajesActionTypes.VOTE_UP: {
            const d: DestinoViaje = (action as VoteUpAction).destino;
            var newDest = new DestinoViaje(d.nombre, d.url, d.votes); 
            newDest.voteUp();
            return { 
                ...state,
                items: [ ...state.items.map(dest => {
                    if (dest == d) {
                        return newDest;
                    } else {
                        return dest;
                    }
                }) ]
             };
        }
        case DestinosViajesActionTypes.VOTE_DOWN: {
            const d: DestinoViaje = (action as VoteDownAction).destino;
            var newDest = new DestinoViaje(d.nombre, d.url, d.votes); 
            newDest.voteDown();
            return { 
                ...state,
                items: [ ...state.items.map(dest => {
                    if (dest == d) {
                        return newDest;
                    } else {
                        return dest;
                    }
                }) ]
             };
        }
        case DestinosViajesActionTypes.VOTE_RESET: {
            const d: DestinoViaje = (action as VoteDownAction).destino;
            var newDest = new DestinoViaje(d.nombre, d.url, d.votes); 
            newDest.voteReset();
            return { 
                ...state,
                items: [ ...state.items.map(dest => {
                    if (dest == d) {
                        return newDest;
                    } else {
                        return dest;
                    }
                }) ]
             };
        }
    }
    return state;
}

//EFFECTS
@Injectable()
export class DestinosViajesEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );

    constructor(private actions$: Actions) { }
}