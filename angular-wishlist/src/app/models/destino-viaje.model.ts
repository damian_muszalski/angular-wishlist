export class DestinoViaje {
    
    public servicios: string[];

    constructor(public nombre: string, public url: string, public votes: number = 0) {
        this.servicios = ['pileta', 'desayuno'];
    }

    voteUp() {
        this.votes++;
    }

    voteDown() {
        this.votes--;
    }

    voteReset() {
        this.votes = 0;
    }
}